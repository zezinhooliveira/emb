/**
 * Created by oliveira on 31/05/16.
 */


var net = require("net");
var bell=require("./models/door_bell");

var HOST = '0.0.0.0';
var PORT = 8080;

var socket =function () {
    net.createServer(function (sock) {

        // We have a connection - a socket object is assigned to the connection automatically
        console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

        // Add a 'data' event handler to this instance of socket
        sock.on('data', function (data) {

            console.log('DATA ' + sock.remoteAddress + ': ' + data);
            // Write the data back to the socket, the client will receive it as data from the server
            obj = JSON.parse(data);

            if (obj.type == "init") {
                token=obj.token;
                bell.init_client(token,obj.door,sock);
                sock.write("logged");
                console.log("hello")
            }


            sock.write('You said "' + data + '"');

        });

        // Add a 'close' event handler to this instance of socket
        sock.on('close', function (data) {
            console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
        });

    }).listen(PORT, HOST);

    console.log('Server listening on ' + HOST + ':' + PORT);
};

module.exports=socket;




/*
var net = require('net');
var stream=require("./stream");

var HOST = '127.0.0.1';
var PORT = 6969;

var client = new net.Socket();
client.connect(PORT, HOST, function() {

    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
    client.write(JSON.stringify({"type":"init","token":123}));
    
    

});

// Add a 'data' event handler for the client socket
// data is what the server sent to this socket
client.on('data', function(data) {

    console.log('DATA: ' + data);
    // Close the client socket completely
    client.destroy();

});

// Add a 'close' event handler for the client socket
client.on('close', function() {
    console.log('Connection closed');
});
    */


