/**
 * Created by oliveira on 31/05/16.
 */
var net = require("net");
var stream= require("./stream");
var bell=require("./models/door_bell");

var HOST = '0.0.0.0';
var PORT = 6969;

var socket =function () {
    net.createServer(function (sock) {

        // We have a connection - a socket object is assigned to the connection automatically
        console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

        // Add a 'data' event handler to this instance of socket
        sock.on('data', function (data) {

            console.log('DATA ' + sock.remoteAddress + ': ' + data);
            // Write the data back to the socket, the client will receive it as data from the server
            obj = JSON.parse(data);

            if (obj.type == "init") {
                if (obj.token == bell.stream_token) {
                    sock.write("nice u are in");
                    s = new stream(sock);

                }
                else {
                    sock.write("u shall not pass");
                }
            }


            sock.write('You said "' + data + '"');

        });

        // Add a 'close' event handler to this instance of socket
        sock.on('close', function (data) {
            console.log('CLOSED: ' + sock.remoteAddress + ' ' + sock.remotePort);
        });

    }).listen(PORT, HOST);

    console.log('Server listening on ' + HOST + ':' + PORT);
};

module.exports=socket;