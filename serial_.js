var spawn= require('child_process').spawn;
var serialport = require('serialport');
var bell= require('./models/door_bell');

//fazer singleton!!!!!!!!!!!!!!!!!
SerialPort = serialport.SerialPort;

count=0;
var iniTime;

var myPort;


var serial = function(){

    this.portName = "/dev/ttyACM0";
	myPort = new SerialPort(this.portName, {
		baudRate: 9600,
		parser: serialport.parsers.readline("\n")
	});



    myPort.on('open', this.showPortOpen);
    myPort.on('data', this.reciveSerialData);
    myPort.on('close', this.showPortClose);
    myPort.on('error', this.showError);
    count=0;
};

 serial.prototype.showPortOpen=  function () {
   console.log('port open. Data rate: '/*+ this.myPort.options.baudRate*/);
};
 
serial.prototype.reciveSerialData= function (data) {


    console.log("recive_arduino: "+data);
    teste = data;


    if(teste.slice(-2).charAt(0)=='A'){
        iniTime=Date.now()/1000/60;
        takePhoto();
    }if(teste.slice(-5).charAt(3)=='C'){
        // "C #number_of_door\n"
        console.log("ola maria joana");
        teste=data.slice(-5);
        pin=teste.substr(0,teste.length-2);
        bell.callNumber(pin);
    }if(teste.slice(-5).charAt(3)=='D'){
        console.log("open door");
        teste=data.slice(-5);
        pin=teste.substr(0,teste.length-2);
        console.log(pin.length);
        if(bell.validadePin(pin)){
            sendSerialData("ok");
        }
        else{
            sendSerialData("no");
        }
    }
};

serial.prototype.sendSerialData= function (data) {

    console.log(data);
    myPort.write(data);
    
    
};

sendSerialData= function (data) {

    console.log(data);
    myPort.write(data);


};

serial.prototype.takePhoto= function (){

	console.log("A tirar foto "+this.count);
	var args = ["-w", "640", "-h", "480", "-o", "./stream/00000"+this.count+".jpg","--nopreview"];
	var proc = spawn('raspistill', args);
	if(this.count<5){
			this.count++;
		setTimeout(takePhoto,7000);}
	else
		this.count=0;

};

takePhoto= function (){

    console.log("A tirar foto "+count);
    var args = ["-w", "640", "-h", "480", "-o", "./stream/originals/00000"+count+".jpg","--nopreview"];
    var proc = spawn('raspistill', args);
    console.log((Date.now()/1000/60)-iniTime);
    if(((Date.now()/1000/60)-iniTime)<5){
        console.log("oi");
        count++;
        setTimeout(takePhoto,7000);}


};

serial.prototype.showPortClose= function () {
   console.log('port closed.');
};

serial.prototype.showError = function (error) {
   console.log('Serial port error: ' + error);
};

module.exports=serial;