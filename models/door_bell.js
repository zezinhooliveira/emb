/**
 * Created by oliveira on 19/05/16.
 */
var Door = require("./door");

var door_bell = function door_bell(){
    //defining a var instead of this (works for variable & function) will create a private definition
    this.doors=[];
    this.tokens=[];
    this.stream_token=null;

    this.add = function (door) {
        this.doors[door.number]=door;
    }

    this.remove= function () {
        this.doors.pop();
    }

    this.init =function (doors) {
        this.doors=doors;
        console.log("done")
    }
    
    this.add_user= function (door,user) {
        this.doors[door].addUser(user);
    };
    
    this.getJSON=function () {
        return JSON.stringify(this);
    };
    
    this.callNumber=function (number) {
        console.log(number);
        if(this.doors[parseInt(number)]!=null){
            this.doors[parseInt(number)].call();
        }
    };
    
    this.init_client=function (token,door,socket){
        console.log(this.doors[door].getJSON());
        this.doors[door].init_client(token,socket);
    };
    this.validadePin=function (pin) {
        for(i =0;i<this.doors.length;i++){

            console.log(this.doors[i].pin +"=="+pin);

            if (this.doors[i].pin==parseInt(pin)){
                return true;
            }
        }
        return false;
    }
    
    this.initJSON= function (doors) {
        bb= JSON.parse(doors);
        for(i=0;i<Object.keys(bb).length;i++){
            if(bb[i]!=null)
                this.doors[bb[i].number]=(new Door(bb[i].users,bb[i].number,bb[i].pin));
        }
        this.tokens=bb.tokens;
    }
}


door_bell.instance= null;

door_bell.getInstance = function(){
    if(this.instance === null){
        this.instance = new door_bell();
    }
    return this.instance;
};



module.exports = door_bell.getInstance();