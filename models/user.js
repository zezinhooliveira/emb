/**
 * Created by oliveira on 24/04/16.
 */

var User = function (user,password,token) {
    this.user=user;
    this.password=password;
    this.token=token;
    this.socket=null;
};

User.prototype.changeUserName=function (user) {
    this.user=user;
};

User.prototype.changePassword=function (password) {
    this.password=password;
};

User.prototype.updateToken=function (token) {
    this.token=token;
}

User.prototype.getJson=function () {
    var use= {user:this.user,password:this.password,token:this.token};
    return JSON.stringify(use);
};

module.exports=User;
