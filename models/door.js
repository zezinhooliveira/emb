/**
 * Created by oliveira on 24/04/16.
 */
var User= require('./user');

var Door = function (users,number,pin) {
    this.number=number;
    this.users=users;
    this.pin=pin;
    this._call=false;
};



Door.prototype.init = function (json) {
    var obj= JSON.parse(json);
    this.number=obj.door;
    this.pin=obj.pin;
    this.users=[];
    
    for(i=0;i<obj.users.length;i++){
        this.users.push(new User(1, obj.users[i].user,obj.users[i].password,obj.users[i].token));
    }
}

Door.prototype.addUser=function (user){
    this.users.push(user);
};

Door.prototype.delUser=function (user) {
    for(i=0;i<this.users.length;i++){
        if(this.users[i]==users){
            delete this.users[i];
        }
    }
};

Door.prototype.init_client=function (token,socket) {
    for(i=0;i<this.users.length;i++){
        if(this.users[i].token==token){
            this.users[i].socket=socket;
        }
    }
};

Door.prototype.call=function () {
    this._call=true;
}

Door.prototype.stop=function () {
    this._call=false;
}

Door.prototype.getJSON=function () {
    var door=[];
    for( var i =0;i<this.users.length;i++){
        door.push(this.users[i]);
    }
    num = this.number;
    json={door:num,pin:this.pin,users:door};
    return JSON.stringify(json);
}

Door.prototype.getUsers = function (){
    return this.users;
}

module.exports=Door;