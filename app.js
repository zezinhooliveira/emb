var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var images= require('./routes/images');
var op = require('./routes/op');
var login= require('./routes/login');
var admin= require('./routes/admin');

var User=require("./models/user");
var Door= require('./models/door');
var bell= require("./models/door_bell");

var db = require("./db.js");
var conf= require("./conf");
var Serial = require("./serial_");
var Stream= require("./stream");
var so_server=require("./socket_server");
var cl_server=require("./socket_cliente");

var spawn = require('child_process').spawn;
var proc;
var fs= require('fs');
var stream= require('stream');



d= new Door([],0,123);
u= new User(conf.device_admin_u,conf.device_admin_p,1);
d.addUser(u);
bell.add(d);

bell.initJSON(db.read());

console.log(bell.getJSON());
//db.write(bell.getJSON());
var serial= new Serial();

//var stream= new Stream();

//stream.start();
//serial.reciveSerialData("P 58\n")

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/images',images);
app.use('/api',op);
app.use('/login',login);
app.use('/admin',admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

kl=app.listen(3001,function () {
    console.log("port 3001");
});

//so_server();
cl_server();

    module.exports = app;
