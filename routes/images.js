/**
 * Created by oliveira on 21/04/16.
 */
var express = require('express');
var router = express.Router();
var fs = require('fs');
var Thumbnail= require('thumbnail');

// image?image=[image]
router.get('/original',function (req,res,next) {
    cne=req.query.image.split('-');
    var img = fs.readFileSync("stream/originals/"+cne[0]+".jpg");
    res.writeHeader(200,{'Content-Type':'image/jpg'});
    res.end(img,'binary')
});

router.get('/thumb',function (req,res,next) {
    var img = fs.readFileSync("stream/thumnails/"+req.query.image);
    res.writeHeader(200,{'Content-Type':'image/jpg'});
    res.end(img,'binary')
});

router.get('/del',function (req, res, next) {

    cne=req.query.image.split('-');
    fs.unlinkSync("stream/originals/"+cne[0]+".jpg");
    fs.unlinkSync("stream/thumnails/"+req.query.image);
    res.writeHeader(200,{'Content-Type':'image/jpg'});
    res.end();
});

router.get('/delAll',function (req, res, next) {

    ls = fs.readdirSync('stream/originals');
    for (i=0;i<ls.length;i++)
        fs.unlinkSync("stream/originals/"+ls[i]);

    ls = fs.readdirSync('stream/thumnails');
    for(i=0;i<ls.length;i++)
        fs.unlinkSync("stream/thumnails/"+ls[i]);
    res.end();
});


router.get('/',function (req,res,next) {


    var thumbnail = new Thumbnail('stream/originals', 'stream/thumnails');

    ls = fs.readdirSync('stream/originals');


    for(i=0;i<ls.length;i++) {
        console.log(ls[i]);
        thumbnail.ensureThumbnail(ls[i], 100, 100, function (err, filename) {

            console.log(filename);

        });
    }

    //add tumbnail
    ls = fs.readdirSync('stream/thumnails');
    res.writeHeader(200,{'Content-Type':'application/json'});
    
    res.end(JSON.stringify(ls));
    
    
});

module.exports=router;
