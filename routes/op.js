/**
 * Created by oliveira on 21/04/16.
 */
var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var bell= require("../models/door_bell");
var crypto= require("crypto");

var connection=mysql.createConnection({
    host     : 'localhost',
    user     : '< MySQL username >',
    password : '< MySQL password >',
    database : '<your database name>'
});
router.get('/',function (req,res,next) {
    res.render("tester");
});
router.get('/open',function (req, res, next) {
    //check the database;
    //res.send("yes|no");
});

router.get('/door',function (req, res) {
    door=req.query.door;

    if(bell.doors[door]._call==true){
        res.writeHeader(200,{'Content-Type':'application/json'});
        res.end(JSON.stringify({"res":200}))
        bell.doors[door]._call=false;
    }
    else {
        res.writeHeader(200,{'Content-Type':'application/json'});
        res.end(JSON.stringify({"res":400}))
    }
})

//correct 
router.get('/call',function (req, res, next) {
    res.send("calling:"+req.query.id);
});

router.get('/stream',function (req, res, next) {
    var token=new Buffer(4);
    crypto.randomBytes(4,function (err,token) {
    });

    bell.stream_token=token.toString("hex");
    res.end(token.toString("hex"))
});


module.exports=router;