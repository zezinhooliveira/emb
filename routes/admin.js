/**
 * Created by oliveira on 24/05/16.
 */
var express = require('express');
var router = express.Router();
var fs= require("../db");
var confs=require("../conf");
var User= require("../models/user");
var Door = require("../models/door");
var bell= require("../models/door_bell");
var crypto= require("crypto");
var db= require("../db");

//login?user=[username],password=[password]
router.get('/',function (req,res,next) {
    res.render('login_admin');
});
router.post('/login',function (req, res, next) {
    var password= req.body.password;
    var user= req.body.login;

    for(i=0;i<bell.doors[0].users.length;i++){
        if(bell.doors[0].users[i].user==user && bell.doors[0].users[i].password==password){
            res.render("register_door");
        }
    }
});

router.get('/door',function (req, res) {
    res.writeHeader(200,{'Content-Type':'application/json'})
    res.end(bell.getJSON());
})

router.post('/door',function (req, res, next) {
    var door= req.body.door;
    var pin = req.body.pin;
    d = new Door([],door,pin);
    bell.doors[door]=d;
    res.writeHeader(200,{'Content-Type':'application/json'})
    res.end(bell.getJSON());
    db.write(bell.getJSON());

 });
 router.get('/logout',function (req, res, next) {
});


module.exports=router;