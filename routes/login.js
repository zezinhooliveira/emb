/**
 * Created by oliveira on 22/04/16.
 */
var express = require('express');
var router = express.Router();
var fs= require("../db");
var confs=require("../conf");
var User= require("../models/user");
var bell= require("../models/door_bell");
var crypto= require("crypto");
var db= require("../db");

//login?user=[username],password=[password]
router.get('/',function (req,res,next) {
    res.render('login');
});
router.post('/login',function (req, res, next) {
    var password= req.body.password;
    var user= req.body.login;
    var door= req.body.door;
    
    for(i=0;i<bell.doors[door].users.length;i++){
       if(bell.doors[door].users[i].user==user && bell.doors[door].users[i].password==password){
           var token=new Buffer(4);
           crypto.randomBytes(4,function (err,token) {
           });
           res.send(token.toString("hex"));
           utk= bell.doors[door].users[i].token;
           bell.tokens=bell.tokens.filter(function (crr) {
               return utk!=crr;
           });
           bell.doors[door].users[i].token=token.toString("hex");
           bell.tokens.push(token.toString("hex"));
           db.write(bell.getJSON());
           console.log(bell.getJSON());
       }
    }
});
router.get('/token',function (req, res) {
    token = req.query.id;
    for(i=0;i<bell.tokens.length;i++){
        if(bell.tokens[i]==token){
            res.end(JSON.stringify({"res":200}));
        }
    }
    res.end(JSON.stringify({"res":400}));
})

router.get('/registe',function (req, res) {
    res.render("register")
})

router.post('/register',function (req, res, next) {
    var password=req.body.password;
    var user_id=req.body.login;
    var device_token=req.body.device_token;
    var door=parseInt(req.body.door);

    if(device_token==confs.device_token){
        var token=new Buffer(4);
        crypto.randomBytes(4,function (err,token) {
        });
        var user= new User(user_id,password,token.toString("hex"));
        bell.add_user(door,user);
        bell.tokens.push(token.toString("hex"));
        db.write(bell.getJSON());
        console.log(bell.getJSON());
        res.send(token.toString("hex"));
        

    }else
        res.send("failed-token incorrect");
    
});
router.get('/logout',function (req, res, next) {
});


module.exports=router;